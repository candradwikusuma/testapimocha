const chai = require('chai')
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;
const { Random } = require('random-js');
const baseUrl = 'https://be.investree.tech';
const pathUrl = '/auth/login/frontoffice';
const pathUrl2 = '/auth/registration/borrower';

let requestRegister;
describe('POST Loggin Borrower',()=>{
   
    beforeEach(async function(){ 
        requestRegister={
            "userType": 1,
            "salutation": "Mr.",
            "fullname": `Fullname ${randomAlphabet()}`,
            "nationality": 104,
            "email": `test-${randomNumber()}@gmail.com`,
            "mobilePrefix": 1,
            "phoneNumber": randomNumber(),
            "username": `username${randomNumber()}`,
            "password": "Brandal17",
            "referralCode": "",
            "agreePrivacy": true,
            "agreeSubscribe": true,
            "captcha": "qa-bypass-captcha"
        }
        
            const response = await chai
                .request(baseUrl)
                .post(pathUrl2)
                .set({
                    'x-investree-key': '7574a0e42176f3766d2429fc77b96a411ec75e7700ed7407f48489e6889f4d76',
                    'x-investree-signature': 'df8aa07026f5a5198ee0e52c217f5bdc8881035f2c1e3fc39994947899d1894c',
                    'x-investree-timestamp': '2022-01-24T16:48:01',
                    'x-investree-token': null
                })
                .send(requestRegister);
            expect(response.status).to.equal(200)
       console.log('Register borrower with valid credential should success');
    //    console.log(requestRegister);

    })
   

    it('Login borrower with valid credential should success',async ()=>{
        // console.log(requestBody);
        const response= await chai
            .request(baseUrl)
            .post(pathUrl)
            .set({
                'x-investree-key': '7574a0e42176f3766d2429fc77b96a411ec75e7700ed7407f48489e6889f4d76',
                'x-investree-signature': 'df8aa07026f5a5198ee0e52c217f5bdc8881035f2c1e3fc39994947899d1894c',
                'x-investree-timestamp': '2022-01-25T00:33:31',
                'x-investree-token': null
            })
            .send({
                "email": "candradwikusuma07@gmail.com",
                "password": "Candradk7",
                "captcha": "qa-bypass-captcha"
            })
// console.log(response);
            expect(response.status).to.equal(200)
            expect(response.body.data.otpVerificationStatus).to.equal(true)

    })

    it('Login borrower with invalid credential should failed',async()=>{
        // console.log(requestBody);
        const response= await chai
            .request(baseUrl)
            .post(pathUrl)
            .set({
                'x-investree-key': '7574a0e42176f3766d2429fc77b96a411ec75e7700ed7407f48489e6889f4d76',
                'x-investree-signature': 'df8aa07026f5a5198ee0e52c217f5bdc8881035f2c1e3fc39994947899d1894c',
                'x-investree-timestamp': '2022-01-25T00:33:31',
                'x-investree-token': null
            })
           
            .send( {
                "email":  `${Math.random().toString(36)}@gmail.com`,
                "password": "Candradk7",
                "captcha": "qa-bypass-captcha"
                
                
            })
            expect(response.status).to.equal(400)
            // console.log(response);
    })
})

function randomNumber(length = 9) {
    const pool = '1234567890';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomNum = new Random().integer(1, 9);
        result = randomNum + result.slice(1, result.length);
    }

    return result;
}

function randomAlphabet(length = 12) {
    const pool = 'abcdefghijklmnopqrstuvwxyz';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomAlpha = new Random().string(1, 25);
        result = randomAlpha + result.slice(1, result.length);
    }

    return result;
}